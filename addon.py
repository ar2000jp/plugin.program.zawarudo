# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import sys
import xbmcgui

from globals import *
from actionmap import *
from dialogs import ActionListenerDialog
from actionhandlers import X11ActionHandler
import launcher
import settings


class Addon():
    def main(self, argv):
        if (len(sys.argv) == 1):
            self.runLauncher()
        elif ((len(sys.argv) == 2) and (sys.argv[1] == "listActionMap")):
            settings.Settings().listActionMap()
        elif ((len(sys.argv) == 2) and (sys.argv[1] == "addToActionMap")):
            settings.Settings().addToActionMap()
        elif ((len(sys.argv) == 2) and (sys.argv[1] == "removeFromActionMap")):
            settings.Settings().removeFromActionMap()
        elif ((len(sys.argv) == 2) and (sys.argv[1] == "resetActionMap")):
            settings.Settings().resetActionMap()
        else:
            raise Exception("Invalid command line parameters.")

    def runLauncher(self):
        if (ADDON.getSetting("muteNavSounds") == "true"):
            xbmc.enableNavSounds(False)

        if (ADDON.getSetting("stopOnStart") == "true"):
            if xbmc.Player().isPlaying():
                xbmc.Player().stop()

        if (ADDON.getSetting("inhibitIdleShutdown") == "true"):
            xbmc.log("Inhibiting idle shutdown")
            xbmc.executebuiltin("InhibitIdleShutdown(true)")

        appBin = ADDON.getSetting("appBin")
        appArgsList = ADDON.getSetting("appArgs1").split() + ADDON.getSetting("appArgs2").split()
        termTimeout = int(ADDON.getSetting("termTimeout"))

        am = ActionMap().getAllMappings()
        ah = X11ActionHandler(am)
        ald = ActionListenerDialog(ah)
        ah.setOnActionQuit(ald.close)

        l = launcher.Launcher(appBin, appArgsList, termTimeout, ald.close)

        l.launch()
        ald.doModal()

        if (ADDON.getSetting("termOnExit") == "true"):
            l.terminate()

        del ald

        if (ADDON.getSetting("muteNavSounds") == "true"):
            xbmc.enableNavSounds(True)

        if (ADDON.getSetting("inhibitIdleShutdown") == "true"):
            xbmc.log("Allowing idle shutdown")
            xbmc.executebuiltin("InhibitIdleShutdown(false)")


if __name__ == "__main__":
    xbmc.log(ADDONNAME + ": Started")
    addonObj = Addon()
    addonObj.main(sys.argv)
    xbmc.log(ADDONNAME + ": Finished")
