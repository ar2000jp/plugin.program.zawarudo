# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import xbmc
import xbmcgui

from globals import *


class ActionListenerDialog(xbmcgui.WindowXMLDialog):
    def __new__(cls, actionHandlerObj, onInitCallback=None, xmlFilename="DialogBusy.xml"):
        return super(ActionListenerDialog, cls).__new__(cls, xmlFilename, "")

    def __init__(self, actionHandlerObj, onInitCallback=None, xmlFilename="DialogBusy.xml"):
        self.handleAction = actionHandlerObj.handleAction
        self.onInitCallback = onInitCallback

    def onInit(self):
        if self.onInitCallback is not None:
            self.onInitCallback()

    def onAction(self, action):
        buttonCode = action.getButtonCode()
        actionId = action.getId()

        xbmc.log(str(self) + ": actionId: " + str(actionId))
        xbmc.log(str(self) + ": buttonCode: " + str(buttonCode))

        if buttonCode == 0:
            actionType = KActionType.actionId
            actionVal = actionId
        else:
            actionType = KActionType.buttonCode
            actionVal = buttonCode

        self.handleAction(actionType, actionVal)
