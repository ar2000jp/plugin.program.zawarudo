# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import os

try:
    from enum import IntEnum
except ImportError:
    class IntEnum():
        pass

import xbmc
import xbmcaddon

ADDON = xbmcaddon.Addon()
ADDONNAME = ADDON.getAddonInfo('name')
ADDONID = ADDON.getAddonInfo('id')
ADDONVERSION = ADDON.getAddonInfo('version')
ADDONPATH = ADDON.getAddonInfo('path')
LIBPATH = xbmc.translatePath(os.path.join(ADDONPATH, 'resources', 'lib'))
DATAPATH = xbmc.translatePath(ADDON.getAddonInfo('profile'))
XMLPATH = xbmc.translatePath(os.path.join(ADDONPATH, 'resources'))


class KActionType(IntEnum):
    buttonCode = 1
    actionId = 2


class AActionType(IntEnum):
    quit = 1
    xKey = 2
