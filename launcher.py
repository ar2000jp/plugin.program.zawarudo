# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import time
import subprocess
import threading
import xbmc

USE_PSUTIL = False
try:
    import psutil

    USE_PSUTIL = True
except ImportError:
    pass


class PsutilLauncher:
    def __init__(self, appBin, appArgs, termTimeout=5, onTerminateCallback=None):
        self.appBin = appBin
        self.appArgs = appArgs
        self.termTimeout = termTimeout
        self.onTerminateCallback = onTerminateCallback

    def launch(self):
        xbmc.log(str(self) + ": Starting (" + self.appBin + ") with args (" + str(self.appArgs) + ")")
        self.proc = psutil.Popen([self.appBin] + self.appArgs)
        xbmc.log(str(self) + ": Started process PID: " + str(self.proc.pid))
        self.procAlive = True
        self.watcher = ProcessWatcher(self.proc, self.onTerminate)
        self.watcher.start()

    def terminate(self):
        if (self.procAlive):
            xbmc.log(str(self) + ": Stopping process PID: " + str(self.proc.pid))
            self._reapChildren()
        else:
            xbmc.log(str(self) + "Process already terminated.")

    def onTerminate(self, proc):
        xbmc.log("process {} terminated with exit code {}".format(proc.pid, proc.returncode))
        self.procAlive = False
        if self.onTerminateCallback is not None:
            xbmc.log("Calling onTerminateCallback " + str(self.onTerminateCallback))
            self.onTerminateCallback()

    def _reapChildren(self):
        "Tries hard to terminate and ultimately kill the process and all its children."

        def printOnTerminate(proc):
            xbmc.log("process {} terminated with exit code {}".format(proc.pid, proc.returncode))

        procs = self.proc.children()
        self.proc.terminate()
        (gone, alive) = psutil.wait_procs([self.proc], timeout=self.termTimeout, callback=printOnTerminate)

        if alive:
            procs.append(self.proc)

        # Send SIGTERM
        for p in procs:
            try:
                p.terminate()
            except psutil.NoSuchProcess:
                pass

        gone, alive = psutil.wait_procs(procs, timeout=self.termTimeout, callback=printOnTerminate)
        if alive:
            # Send SIGKILL
            for p in alive:
                xbmc.log("process {} survived SIGTERM; trying SIGKILL".format(str(p.pid)))
                try:
                    p.kill()
                except psutil.NoSuchProcess:
                    pass

            gone, alive = psutil.wait_procs(alive, timeout=self.termTimeout, callback=printOnTerminate)
            if alive:
                # Give up
                for p in alive:
                    xbmc.log("process {} survived SIGKILL; giving up".format(str(p.pid)))


class SubprocessLauncher:
    def __init__(self, appBin, appArgs, termTimeout=5, onTerminateCallback=None):
        self.appBin = appBin
        self.appArgs = appArgs
        self.termTimeout = termTimeout
        self.onTerminateCallback = onTerminateCallback

    def launch(self):
        xbmc.log(str(self) + ": Starting (" + self.appBin + ") with args (" + str(self.appArgs) + ")")
        self.proc = subprocess.Popen([self.appBin] + self.appArgs, close_fds=True)
        xbmc.log(str(self) + ": Started process PID: " + str(self.proc.pid))
        self.procAlive = True
        self.watcher = ProcessWatcher(self.proc, self.onTerminate)
        self.watcher.start()

    def terminate(self):
        "Tries hard to terminate and ultimately kill the process."

        if not self.procAlive:
            xbmc.log(str(self) + "Process already terminated.")
            return
        else:
            xbmc.log(str(self) + ": Stopping process PID: " + str(self.proc.pid))

        # Send SIGTERM
        self.proc.terminate()

        for i in range(self.termTimeout + 1):
            if self.proc.poll() != None:
                self.procAlive = False
                break
            time.sleep(1)

        if not self.procAlive:
            return

        # Send SIGKILL
        xbmc.log("process {} survived SIGTERM; trying SIGKILL".format(str(p.pid)))
        self.proc.kill()

        for i in range(self.termTimeout + 1):
            if self.proc.poll() != None:
                self.procAlive = False
                break
            time.sleep(1)

        if not self.procAlive:
            return

        # Give up
        xbmc.log("process {} survived SIGKILL; giving up".format(str(p.pid)))

    def onTerminate(self, proc):
        xbmc.log("process {} terminated with exit code {}".format(proc.pid, proc.returncode))
        self.procAlive = False
        if self.onTerminateCallback is not None:
            xbmc.log("Calling onTerminateCallback " + str(self.onTerminateCallback))
            self.onTerminateCallback()


if (USE_PSUTIL):
    Launcher = PsutilLauncher
else:
    Launcher = SubprocessLauncher


class ProcessWatcher(threading.Thread):
    def __init__(self, proc, onTerminateCallback):
        threading.Thread.__init__(self)
        self.daemon = True
        self.proc = proc
        self.onTerminate = onTerminateCallback

    def run(self):
        while True:
            time.sleep(1)
            if self.proc.poll() != None:
                break

        self.onTerminate(self.proc)
