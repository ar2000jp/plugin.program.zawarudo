# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import os
import xml.etree.ElementTree as ET
from xml.dom import minidom

from globals import *


class ActionMap():
    def __init__(self):
        self.elementTree = ET.ElementTree()
        self.elementTree.parse(os.path.join(XMLPATH, "actionmap.xml"))
        self.actionMapXmlRoot = self.elementTree.getroot()
        if self.actionMapXmlRoot is None:
            raise Exception("Couldn't get ActionMap XML root.")

    def _writeToDisk(self):
        etStr = ET.tostring(self.actionMapXmlRoot, encoding="utf-8")
        etStr = " ".join(etStr.split())
        xmlStr = minidom.parseString(etStr).toprettyxml(encoding="utf-8")
        with open(os.path.join(XMLPATH, "actionmap.xml"), "w") as f:
            f.write(xmlStr.encode("utf-8"))

    def _writeMappings(self, mappingsDict):
        for k, v in mappingsDict.iteritems():
            if type(k[0]) != type(KActionType.actionId):
                raise Exception("Bad type.")
            if type(v[0]) != type(AActionType.xKey):
                raise Exception("Bad type.")

            if type(k[1]) != type(int(1)):
                raise Exception("Bad type.")
            if type(v[1]) != type(int(1)):
                raise Exception("Bad type.")

        for amr in self.actionMapXmlRoot.findall("mapping"):
            self.actionMapXmlRoot.remove(amr)

        for k, v in mappingsDict.iteritems():
            se = ET.SubElement(self.actionMapXmlRoot, "mapping")
            kat = str(int(k[0]))
            kav = str(int(k[1]))
            aat = str(int(v[0]))
            aav = str(int(v[1]))
            comment = str(v[2])
            se.set("kActionType", kat)
            se.set("kActionValue", kav)
            se.set("aActionType", aat)
            se.set("aActionValue", aav)
            se.set("comment", comment)

        self._writeToDisk()

    def getAllMappings(self):
        am = dict()
        for amr in self.actionMapXmlRoot.findall("mapping"):
            aatId = int(amr.get("aActionType"))
            aav = int(amr.get("aActionValue"))
            katId = int(amr.get("kActionType"))
            kav = int(amr.get("kActionValue"))
            comment = amr.get("comment")

            if katId == KActionType.buttonCode:
                kat = KActionType.buttonCode
            elif katId == KActionType.actionId:
                kat = KActionType.actionId
            else:
                raise Exception("Unknown KActionType in action map XML")

            if aatId == AActionType.quit:
                aat = AActionType.quit
                aav = 1
            elif aatId == AActionType.xKey:
                aat = AActionType.xKey
            else:
                raise Exception("Unknown AActionType in action map XML")

            am[(kat, kav)] = (aat, aav, comment)

        return am

    def putAllMappings(self, mappingsDict):
        am = self.getAllMappings()
        am.update(mappingsDict)
        self._writeMappings(am)

    def removeMapping(self, mappingDict):
        for amr in self.actionMapXmlRoot.findall("mapping"):
            aatId = int(amr.get("aActionType"))
            aav = int(amr.get("aActionValue"))
            katId = int(amr.get("kActionType"))
            kav = int(amr.get("kActionValue"))
            comment = amr.get("comment")

            if katId == KActionType.buttonCode:
                kat = KActionType.buttonCode
            elif katId == KActionType.actionId:
                kat = KActionType.actionId
            else:
                raise Exception("Unknown KActionType in action map XML")

            if aatId == AActionType.quit:
                aat = AActionType.quit
                aav = 1
            elif aatId == AActionType.xKey:
                aat = AActionType.xKey
            else:
                raise Exception("Unknown AActionType in action map XML")

            if ((kat, kav) in mappingDict) and (mappingDict[(kat, kav)] == (aat, aav, comment)):
                self.actionMapXmlRoot.remove(amr)
                self._writeToDisk()
                return True

        return False
