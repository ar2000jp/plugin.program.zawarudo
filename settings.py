# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import os
import sys

import xbmc
import xbmcaddon
import xbmcgui
import xbmcvfs
from collections import OrderedDict

from globals import *
from actionmap import *
from actionhandlers import CaptureActionHandler, DummyActionHandler
from dialogs import ActionListenerDialog

sys.path.append(LIBPATH)

from Xlib import X, XK, display
from Xlib.ext import record
from Xlib.protocol import rq


class Settings:
    def listActionMap(self):
        am = ActionMap().getAllMappings()
        aml = list()
        for k, v in am.iteritems():
            if k[0] is KActionType.buttonCode:
                katStr = "ButtonCode"
            elif k[0] is KActionType.actionId:
                katStr = "ActionId"
            else:
                raise Exception("Unknown action type.")

            if v[0] is AActionType.quit:
                aatStr = "Quit"
            elif v[0] is AActionType.xKey:
                aatStr = "X11 KeyCode"
            else:
                raise Exception("Unknown action type.")

            aml.append("Kodi " + katStr + ": " + str(k[1]) + ", " + aatStr + ": " + str(v[1]) +
                       ", comment: " + str(v[2]))
        xbmcgui.Dialog().select("List of mappings", aml)

    def addToActionMap(self):
        choice = xbmcgui.Dialog().select("Choose mapping type",
                                         ["Quit app", "X11 Keyboard key"])

        if choice == 0:
            aat = AActionType.quit
            aav = 1
            comment = "KodiAction -> Quit"
        elif choice == 1:
            aat = AActionType.xKey
            comment = "KodiAction -> "
        elif choice < 0:
            return
        else:
            raise Exception("Unknown action type.")

        def kcdOnInit():
            cald.getControl(400).setImage(os.path.join(ADDONPATH, "resources", "icon.png"))
            cald.getControl(401).addLabel("Perform Kodi action")
            cald.getControl(402).setText("Press the button to be mapped.")

        cah = CaptureActionHandler()
        cald = ActionListenerDialog(cah, kcdOnInit, "DialogNotification.xml")
        cah.setOnActionQuit(cald.close)

        cald.doModal()

        def xcdOnInit():
            dald.getControl(400).setImage(os.path.join(ADDONPATH, "resources", "icon.png"))
            dald.getControl(401).addLabel("Press Keyboard key")
            xec.captureEvent()
            dald.close()

        if aat == AActionType.xKey:
            xec = X11EventCapturer()
            dah = DummyActionHandler()
            dald = ActionListenerDialog(dah, xcdOnInit, "DialogNotification.xml")
            dah.setOnActionQuit(dald.close)
            dald.doModal()

            aav = xec.getCapturedEvent()
            keyStr = xec.lookupKeyName(aav)
            comment += keyStr

        comment = xbmcgui.Dialog().input("Enter comment", defaultt=comment)

        if not comment or comment == "":
            return

        kat, kav = cah.getCapturedAction()

        if kat is KActionType.buttonCode:
            katStr = "ButtonCode"
        elif kat is KActionType.actionId:
            katStr = "ActionId"
        else:
            raise Exception("Unknown action type.")

        if aat is AActionType.quit:
            aatStr = "Quit"
        elif aat is AActionType.xKey:
            aatStr = "X11 KeyCode"
        else:
            raise Exception("Unknown action type.")

        amStr = ("Kodi " + katStr + ": " + str(kav) + ", " + aatStr + ": " + str(aav) +
                 ", comment: " + str(comment))

        am = dict()
        am[(kat, kav)] = (aat, aav, comment)

        if xbmcgui.Dialog().yesno("Add mapping?", "This will add the action mapping:\n" +
                                                  amStr + "\nAre you sure?"):

            try:
                ActionMap().putAllMappings(am)
                xbmcgui.Dialog().ok("Success", "Action mapping added successfully.")
            except Exception:
                xbmcgui.Dialog().ok("Failed", "Failed to add action mapping.")

    def removeFromActionMap(self):
        oam = OrderedDict(ActionMap().getAllMappings())
        aml = list()
        for k, v in oam.iteritems():
            if k[0] is KActionType.buttonCode:
                katStr = "ButtonCode"
            elif k[0] is KActionType.actionId:
                katStr = "ActionId"
            else:
                raise Exception("Unknown action type.")

            if v[0] is AActionType.quit:
                aatStr = "Quit"
            elif v[0] is AActionType.xKey:
                aatStr = "X11 KeyCode"
            else:
                raise Exception("Unknown action type.")

            aml.append("Kodi " + katStr + ": " + str(k[1]) + ", " + aatStr + ": " + str(v[1]) +
                       ", comment: " + str(v[2]))
        choice = xbmcgui.Dialog().select("List of mappings", aml)
        if choice < 0:
            return

        ram = {oam.keys()[choice]: oam[oam.keys()[choice]]}
        if xbmcgui.Dialog().yesno("Remove mapping?", "This will remove the action mapping:\n" +
                                                     aml[choice] + "\nAre you sure?"):

            success = ActionMap().removeMapping(ram)
            if success:
                xbmcgui.Dialog().ok("Success", "Action mapping removed successfully.")
            else:
                xbmcgui.Dialog().ok("Failed", "Failed to remove action mapping.")

    def resetActionMap(self):
        if xbmcgui.Dialog().yesno("Reset?", "This will reset the action map to "
                                            "defaults.\nAre you sure?"):
            srcPath = os.path.join(ADDONPATH, "resources", "actionmap.xml.default")
            dstPath = os.path.join(ADDONPATH, "resources", "actionmap.xml")

            success = xbmcvfs.copy(srcPath, dstPath)
            if success:
                xbmcgui.Dialog().ok("Success", "Action map reset successfully.")
            else:
                xbmcgui.Dialog().ok("Failed", "Failed to reset action map.")


class X11EventCapturer():
    def __init__(self):
        self.localDisplay = display.Display()
        self.recordDisplay = display.Display()

        # Check if the extension is present
        if not self.recordDisplay.has_extension("RECORD"):
            raise Exception("RECORD X11 extension not found")

        r = self.recordDisplay.record_get_version(0, 0)
        xbmc.log(str(self) + ": RECORD extension version {:d}.{:d}".format(r.major_version, r.minor_version))

    def getCapturedEvent(self):
        return self.capturedEvent

    def captureEvent(self):
        # Create a recording context; we only want key events
        self.recordContext = self.recordDisplay.record_create_context(
            0,
            [record.AllClients],
            [{
                'core_requests': (0, 0),
                'core_replies': (0, 0),
                'ext_requests': (0, 0, 0, 0),
                'ext_replies': (0, 0, 0, 0),
                'delivered_events': (0, 0),
                'device_events': (X.KeyPress, X.KeyRelease),
                'errors': (0, 0),
                'client_started': False,
                'client_died': False,
            }])

        # Enable the context; this only returns after a call to record_disable_context,
        # while calling the callback function in the meantime
        self.recordDisplay.record_enable_context(self.recordContext, self.recordCallback)

        # Finally free the context
        self.recordDisplay.record_free_context(self.recordContext)

        return self.capturedEvent

    def lookupKeyName(self, xKeyCode):
        keySymbol = self.localDisplay.keycode_to_keysym(xKeyCode, 0)
        if not keySymbol:
            return None
        for name in dir(XK):
            if name[:3] == "XK_" and getattr(XK, name) == keySymbol:
                return name[3:]

    def recordCallback(self, reply):
        if reply.category != record.FromServer:
            return
        if reply.client_swapped:
            xbmc.log(str(self) + ": Received swapped protocol data, cowardly ignored.")
            return
        if not len(reply.data) or reply.data[0] < 2:
            # Not an event
            return

        data = reply.data
        while len(data):
            event, data = rq.EventField(None).parse_binary_value(data,
                                                                 self.recordDisplay.display, None, None)

            if event.type is X.KeyPress:
                xbmc.log(str(self) + ": xKeyCode {:#04x}".format(event.detail))

                self.capturedEvent = event.detail
                self.localDisplay.record_disable_context(self.recordContext)
                self.localDisplay.flush()
                return
