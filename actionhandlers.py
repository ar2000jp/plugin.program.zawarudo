# -*- coding: utf-8 -*-

#
# Copyright 2018,
# Ahmad Draidi and the ZA WARUDO! addon contributors
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Python 2/3 compatibility.
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division

import sys

from globals import *
from actionmap import *

sys.path.append(LIBPATH)

from Xlib import X, display
from Xlib.ext import xtest


class X11ActionHandler():
    def __init__(self, actionMapDict):
        self.actionMapDict = actionMapDict
        self.dpy = display.Display()

        # Check if the extension is present
        if not self.dpy.has_extension("XTEST"):
            raise Exception("XTEST extension not found")

        r = self.dpy.xtest_get_version(0, 0)
        xbmc.log(str(self) + ": XTEST extension version %d.%d" % (r.major_version, r.minor_version))

        # TODO: This shouldn't be here. Implement properly.
        if (ADDON.getSetting("moveMouseAway") == "true"):
            xtest.fake_input(self.dpy, X.MotionNotify, x=10000, y=10000)
            self.dpy.sync()

    def setOnActionQuit(self, onActionQuit):
        self.onActionQuit = onActionQuit

    def handleAction(self, kActionType, kActionVal):
        xbmc.log(str(self) + ": kActionVal: " + str(kActionVal))
        xbmc.log(str(self) + ": kActionType: " + str(kActionType))

        if (kActionType, kActionVal) in self.actionMapDict:
            (aat, aav, comment) = self.actionMapDict[(kActionType, kActionVal)]
        else:
            xbmc.log(str(self) + ": Unhandled action kActionType: " + str(kActionType) +
                     ", kActionVal: " + str(kActionVal))
            return

        if aat == AActionType.quit:
            xbmc.log(str(self) + ": Quit action received.")
            self.onActionQuit()
            return

        elif aat == AActionType.xKey:
            xbmc.log(str(self) + ": xKeyCode: " + str(aav))

            xtest.fake_input(self.dpy, X.KeyPress, aav)
            xtest.fake_input(self.dpy, X.KeyRelease, aav)
            self.dpy.sync()
        else:
            raise Exception("Unknown AActionType.")


class DummyActionHandler():
    def __init__(self):
        pass

    def setOnActionQuit(self, onActionQuit):
        self.onActionQuit = onActionQuit

    def handleAction(self, kActionType, kActionVal):
        self.onActionQuit()


class CaptureActionHandler():
    def __init__(self):
        pass

    def setOnActionQuit(self, onActionQuit):
        self.onActionQuit = onActionQuit

    def getCapturedAction(self):
        return self.capturedAction

    def handleAction(self, kActionType, kActionVal):
        xbmc.log(str(self) + ": kActionVal: " + str(kActionVal))
        xbmc.log(str(self) + ": kActionType: " + str(kActionType))

        self.capturedAction = (kActionType, kActionVal)
        self.onActionQuit()
