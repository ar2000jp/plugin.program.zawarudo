# ZA WARUDO!
A Kodi launcher add-on that controls the launched app through Kodi buttons/actions.

### Description
ZA WARUDO! is a launcher that can remap Kodi actions and button codes to X11 
events after launching an application. This allows you to control apps using 
Kodi's Android remote app, Kore, among other things.

This addon uses [python-Xlib](https://github.com/python-xlib/python-xlib) and 
the X11 XTEST extension to send key strokes and other events to launched apps. 
It also uses [psutil](https://github.com/giampaolo/psutil) to manage the launched app.

Icon is launch by coloripop from the Noun Project.

### License
Copyright 2018 Ahmad Draidi and the ZA WARUDO! addon contributors.  
SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

